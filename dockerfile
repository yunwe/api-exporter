FROM python:3.8.6
WORKDIR /data/api-exporter
COPY ./api-exporter /data/api-exporter
RUN pip3 install -i https://pypi.douban.com/simple -r requirements.txt
EXPOSE 8000
CMD ["python", "app.py"]