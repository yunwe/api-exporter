# 找到替代方案，暂时废弃，代码不可用
## 项目说明
Prometheus提供的blackbox模块不怎么好用，配置挺麻烦，所以写了个自定义的API监控模块，修改配置文件生效需要重启服务。
## 配置说明
```yaml
#如果http启用了安全认证，则在 ↓ 设置账号密码,实例：
#basic_auth:
#    username: ""
#    password: ""
#group2:                                   /* 必须，组名称 */
#  百度:                                    /* 必须，url别名 */
#    url: "http://httpbin.org/html"        /* 必须，url地址 */
#    result: "asdfasdfsd"                  /* 非必须，要求的返回参数，支持正则 */
#    type: "get"                           /* 必须，请求类型(get|post) */
#    timeout: 5                            /* 非必须，请求超时时间，默认为5s */
#    body: {                               /* 非必须，请求参数 */
#      "success":"false",
#      "message":"失败原因"
#    }


group2:
  百度:
    url: "http://httpbin.org/html"
    result: "asdfasdfsd"
    type: "get"
    body: {
      "success":"false",
      "message":"失败原因"
    }
  http:
    url: "http://httpbin.org/get"
    type: "get"
group3:
  阿里:
    url: "http://httpbin.org/xml"
    result: "all"
    type: "get"
```
支持多配置文件，在 **config** 目录添加yaml格式的配置即可。
## 部署
本地部署自行百度，本教程只提供Kubernetes或Docker环境部署
dockerfile文件
```shell script
FROM python:3.8.6
WORKDIR /data/api-exporter
COPY ./api-exporter /data/api-exporter
RUN pip3 install -i https://pypi.douban.com/simple -r requirements.txt
EXPOSE 8000
CMD ["python", "app.py"]
```
kubernetes相关文件
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: get-api
  namespace: monitoring
data:
  api.yaml: |-
    group2:
      百度:
        url: "http://httpbin.org/html"
        result: "asdfasdfsd"
        type: "get"
        body: {
          "success":"false",
          "message":"失败原因"
        }
      http:
        url: "http://httpbin.org/get"
        type: "get"
    group3:
      阿里:
        url: "http://httpbin.org/xml"
        result: "all"
        type: "get"
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    name: api-exporter
  name: api-exporter
  namespace: monitoring
spec:
  replicas: 1
  selector:
    matchLabels:
      name: api-exporter
  template:
    metadata:
      labels:
        name: api-exporter
    spec:
      containers:
      - env:
        - name: TZ
          value: Asia/Shanghai
        image: registry.cn-hangzhou.aliyuncs.com/huang-image/api-exporter:v0.15
        imagePullPolicy: IfNotPresent
        name: api-exporter
        ports:
        - containerPort: 5000
          name: 5000tcp
          protocol: TCP
        volumeMounts:
        - mountPath: "/data/api-exporter/api.yaml"
          name: api-list
          subPath: api.yaml
      volumes:
      - name: api-list
        configMap:
          name: get-api
      dnsConfig:
        nameservers:
        - 114.114.114.114
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: api-exporter
  name: api-exporter
  namespace: monitoring
spec:
  ports:
  - name: http
    port: 5000
    protocol: TCP
    targetPort: 5000
  selector:
    name: api-exporter
  sessionAffinity: None
  type: ClusterIP
```
